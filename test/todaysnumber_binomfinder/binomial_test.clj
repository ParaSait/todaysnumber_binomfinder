(ns todaysnumber-binomfinder.binomial-test
  (:require [midje.sweet :refer :all]
            [todaysnumber-binomfinder.binomial :refer :all]))

(facts "about `binom`"
       (fact "calculates binomial probability of X = x"
             (binom 3 10 0.1) => (roughly 0.057395628)))

(facts "about `binom>=`"
       (fact "calculates binomial probability of X >= x"
             (binom>= 3 10 0.1) => (roughly 0.0701908264))

       (fact "calculating the most extreme use case that the factorial allows succeeds"
             (binom>= 10 20 0.1) => anything))