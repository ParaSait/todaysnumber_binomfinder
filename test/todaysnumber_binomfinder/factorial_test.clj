(ns todaysnumber-binomfinder.factorial-test
  (:require [midje.sweet :refer :all]
            [todaysnumber-binomfinder.factorial :refer :all]))

(facts "about `!`"
       (fact "calculates factorials"
             (! 0) => 1
             (! 5) => 120
             (! 10) => 3628800)

       (fact "a long overflow is expected at >20"
             (! 20) => 2432902008176640000
             (! 21) => (throws java.lang.ArithmeticException)))
