(ns todaysnumber-binomfinder.finder-test
  (:require [midje.sweet :refer :all]
            [todaysnumber-binomfinder.finder :refer :all]
            [todaysnumber-binomfinder.binomial :refer :all]))

(facts "about `gather-indices`"
       (fact "gathers indices of a given number within a pick sequence"
             (gather-indices [2 1 1 2 1 2 2 1 2] 1) => [1 2 4 7]
             (gather-indices [2 1 1 2 1 2 2 1 2] 2) => [0 3 5 6 8]
             (gather-indices [2 1 1 2 1 2 2 1 2] 3) => []))

(facts "about `gather-index-ranges`"
       (fact "gathers index ranges to be tested on some pick sequence for a given number"
             (gather-index-ranges [2 1 1 2 1 2 2 1 2] 1)
             => (just [[1 1]
                       [1 2]
                       [1 4]
                       [1 7]
                       [2 2]
                       [2 4]
                       [2 7]
                       [4 4]
                       [4 7]
                       [7 7]]
                      :in-any-order)
             (gather-index-ranges [2 1 1 2 1 2 2 1 2] 3)
             => []))

(facts "about `gather-nwrs`"
       (fact "gathers the number-within-ranges (a tuple containing a number + an index range) to perform a calculation for"
             (gather-nwrs [2 1 2 1 1 2])
             => (just [[1 [1 1]]
                       [1 [1 3]]
                       [1 [1 4]]
                       [1 [3 3]]
                       [1 [3 4]]
                       [1 [4 4]]
                       [2 [0 0]]
                       [2 [0 2]]
                       [2 [0 5]]
                       [2 [2 2]]
                       [2 [2 5]]
                       [2 [5 5]]]
                      :in-any-order)))

(facts "about `calc-nwr-binom>=`"
       (fact "calculates the binomial probability for X >= x for a given number-within-range over a pick sequence"
             (calc-nwr-binom>= [1 [1 7]] [2 1 1 2 1 2 2 1 2])
             => (binom>= 4 7 0.1)))

(facts "about `calc-nwrb>=`"
       (fact "converts an nwr into an nwrb>= given a pick sequence"
             (calc-nwrb>= [1 [1 4]] [2 1 2 1 1 2])
             => [[1 [1 4]] (binom>= 3 4 0.1)]))

(def lynch-seq
                                   [5
                                    1
                                    9
                                    3
                                    2
                                    6
                                    7
                                    8
                                    3
                                    9
                                    1
                                    10
                                    1
                                    8
                                    4
                                    4
                                    9
                                    2
                                    6
                                    7
                                    10
                                    7
                                    8
                                    3
                                    9
                                    7
                                    3
                                    6
                                    10
                                    5
                                    1
                                    6
                                    10
                                    7
                                    8
                                    10
                                    5
                                    6
                                    8
                                    6
                                    9
                                    10
                                    2
                                    4
                                    9
                                    4
                                    3
                                    4
                                    10
                                    6
                                    6
                                    1
                                    7
                                    10
                                    3
                                    1
                                    6
                                    3
                                    6
                                    9
                                    7
                                    9
                                    7
                                    9
                                    6
                                    5
                                    10
                                    8
                                    7
                                    5
                                    9
                                    8
                                    5
                                    5
                                    4
                                    7
                                    7
                                    4
                                    6
                                    1
                                    10
                                    8
                                    9
                                    7
                                    8
                                    6
                                    6
                                    6
                                    3
                                    6
                                    7
                                    7
                                    2
                                    10
                                    6
                                    1
                                    5
                                    2
                                    7
                                    10
                                    2
                                    2
                                    7
                                    3
                                    1
                                    5
                                    9
                                    10
                                    2
                                    5
                                    3
                                    9
                                    9
                                    4
                                    3
                                    8
                                    8
                                    6
                                    3
                                    2
                                    5
                                    1
                                    2
                                    7
                                    7
                                    4
                                    2
                                    6
                                    7
                                    6
                                    6
                                    8
                                    1
                                    5
                                    2
                                    6
                                    5
                                    5
                                    6
                                    5
                                    4
                                    10
                                    3
                                    1
                                    1
                                    3
                                    3
                                    3
                                    4
                                    6
                                    5
                                    10
                                    4
                                    3
                                    4
                                    6
                                    5
                                    7
                                    1
                                    3
                                    4
                                    6
                                    6
                                    9
                                    6
                                    4
                                    7
                                    8
                                    1
                                    7
                                    6
                                    3
                                    4
                                    5
                                    7
                                    7
                                    10
                                    10
                                    7
                                    6
                                    8
                                    8
                                    5
                                    10
                                    2
                                    8
                                    7
                                    5
                                    6
                                    2
                                    10
                                    10
                                    2
                                    8
                                    4
                                    4
                                    3
                                    10
                                    3
                                    6
                                    3
                                    5
                                    8
                                    5
                                    10
                                    5
                                    4
                                    4
                                    10
                                    6
                                    5
                                    4
                                    6
                                    6
                                    3
                                    2
                                    8
                                    4
                                    8
                                    10
                                    10
                                    8
                                    2
                                    8
                                    8
                                    3
                                    10
                                    10
                                    3
                                    4
                                    5
                                    8
                                    10
                                    2
                                    1
                                    4
                                    7
                                    6
                                    10
                                    4
                                    7
                                    6
                                    2
                                    1
                                    10
                                    4
                                    3
                                    9
                                    8
                                    5
                                    2
                                    9
                                    10
                                    1
                                    3
                                    8
                                    10
                                    8
                                    6
                                    2
                                    9
                                    7
                                    7
                                    8
                                    10
                                    10
                                    9
                                    3
                                    6
                                    5
                                    3
                                    4
                                    2
                                    2
                                    8
                                    8
                                    4
                                    5
                                    5
                                    1
                                    10
                                    2
                                    6
                                    9
                                    5
                                    6
                                    6
                                    9
                                    4
                                    3
                                    6
                                    6
                                    3
                                    3
                                    4
                                    10
                                    4
                                    1
                                    7
                                    5
                                    4
                                    6
                                    10
                                    1
                                    9
                                    5
                                    6
                                    5
                                    6
                                    6
                                    8
                                    6
                                    4
                                    5
                                    9
                                    4
                                    5
                                    4
                                    10
                                    8
                                    8
                                    7
                                    3
                                    6
                                    8
                                    5
                                    3
                                    2
                                    1
                                    6
                                    10
                                    5
                                    4
                                    5
                                    4
                                    4
                                    8
                                    7
                                    4
                                    4
                                    2
                                    3
                                    8
                                    1
                                    5
                                    5
                                    5
                                    8
                                    1
                                    2
                                    1
                                    3
                                    5
                                    8
                                    5
                                    7
                                    1
                                    8
                                    1
                                    2
                                    6
                                    9
                                    6
                                    7
                                    7
                                    5
                                    8
                                    9
                                    3
                                    7
                                    6
                                    5
                                    5
                                    10
                                    3
                                    2
                                    8
                                    10
                                    4
                                    10
                                    3
                                    7
                                    10
                                    2
                                    7
                                    7
                                    2
                                    8
                                    6
                                    2
                                    7
                                    2
                                    3
                                    4
                                    9
                                    3
                                    7
                                    2
                                    4
                                    3
                                    1
                                    2
                                    3
                                    9
                                    5
                                    10
                                    7
                                    10
                                    10
                                    5
                                    1
                                    1
                                    9
                                    2
                                    7
                                    1
                                    5
                                    7
                                    8
                                    2
                                    6
                                    9
                                    10
                                    5
                                    2
                                    1
                                    9
                                    7
                                    7
                                    1
                                    1
                                    4
                                    8
                                    1
                                    5
                                    7
                                    7
                                    1
                                    5
                                    8
                                    3
                                    3
                                    5
                                    4
                                    2
                                    5
                                    7
                                    7
                                    4
                                    3
                                    8
                                    8
                                    1
                                    9
                                    5
                                    8
                                    3
                                    1
                                    6
                                    7
                                    6
                                    3
                                    4
                                    7
                                    1
                                    9
                                    8
                                    3
                                    9
                                    10
                                    3
                                    5
                                    8
                                    10
                                    3
                                    7
                                    3
                                    10
                                    1
                                    7
                                    4
                                    7
                                    3
                                    5
                                    3
                                    3
                                    3
                                    4
                                    2
                                    5
                                    5
                                    9
                                    7
                                    1
                                    9
                                    7
                                    8
                                    6
                                    6
                                    9
                                    2
                                    10
                                    4
                                    10
                                    9
                                    9
                                    7
                                    8
                                    10
                                    5
                                    9
                                    5
                                    3
                                    3
                                    5
                                    5
                                    1
                                    6
                                    6
                                    2
                                    10
                                    1
                                    2
                                    8
                                    3
                                    8
                                    5
                                    2
                                    10
                                    8
                                    4
                                    3
                                    9
                                    9
                                    4
                                    7
                                    3
                                    7
                                    6
                                    5
                                    1
                                    8
                                    9
                                    6
                                    1
                                    8
                                    3
                                    5
                                    7
                                    1
                                    4
                                    3
                                    7
                                    2
                                    3
                                    4
                                    10
                                    6
                                    5
                                    1
                                    1
                                    8
                                    3
                                    5
                                    8
                                    6
                                    9
                                    5
                                    3
                                    5
                                    8
                                    2
                                    2
                                    3
                                    8
                                    9
                                    1
                                    1
                                    9
                                    7
                                    4
                                    8
                                    5
                                    9
                                    8
                                    3
                                    8
                                    1
                                    10
                                    9
                                    8
                                    7
                                    5
                                    9
                                    8
                                    2
                                    2
                                    2
                                    2
                                    7
                                    3
                                    9
                                    3
                                    2
                                    5
                                    6
                                    7
                                    9
                                    10
                                    7
                                    1
                                    5
                                    8
                                    1
                                    1
                                    6
                                    3
                                    9
                                    10
                                    2
                                    3
                                    9
                                    1
                                    6
                                    9
                                    10
                                    9
                                    3
                                    6
                                    6
                                    5
                                    2
                                    2
                                    4
                                    5
                                    9
                                    10
                                    7
                                    7
                                    7
                                    9
                                    3
                                    5
                                    5
                                    7
                                    6
                                    3
                                    10
                                    8
                                    1
                                    1
                                    1
                                    8
                                    7
                                    7
                                    9
                                    6
                                    8
                                    4
                                    7
                                    9
                                    4
                                    1
                                    2
                                    3
                                    3
                                    3
                                    3
                                    5
                                    7
                                    5
                                    5
                                    6
                                    1
                                    9
                                    1
                                    9
                                    9
                                    9
                                    6
                                    9
                                    2
                                    9
                                    9])

(facts "about `calc-all-nwrb>=s-in`"
       (fact "calculates all binom>=-for-number-in-ranges within a given pick sequence"
             (calc-all-nwrb>=s-in [2 1 2 1 1 2])
             => (just [(just [1 [1 1]] (roughly (binom>= 1 1 0.1)))
                       (just [1 [1 3]] (roughly (binom>= 2 3 0.1)))
                       (just [1 [1 4]] (roughly (binom>= 3 4 0.1)))
                       (just [1 [3 3]] (roughly (binom>= 1 1 0.1)))
                       (just [1 [3 4]] (roughly (binom>= 2 2 0.1)))
                       (just [1 [4 4]] (roughly (binom>= 1 1 0.1)))
                       (just [2 [0 0]] (roughly (binom>= 1 1 0.1)))
                       (just [2 [0 2]] (roughly (binom>= 2 3 0.1)))
                       (just [2 [0 5]] (roughly (binom>= 3 6 0.1)))
                       (just [2 [2 2]] (roughly (binom>= 1 1 0.1)))
                       (just [2 [2 5]] (roughly (binom>= 2 4 0.1)))
                       (just [2 [5 5]] (roughly (binom>= 1 1 0.1)))]
                      :in-any-order))

       (future-fact "calculates nwrb>=s for the real david lynch sequence"
             (dorun
              (calc-all-nwrb>=s-in lynch-seq
                                   (fn [current total]
                                     (println (str "Calculating: " current " / " total)))))))

(facts "about `top-nwrb>=s-in`"
       (fact "just calculate it lol"
             (dorun
              (map-indexed (fn [idx [[number [lower upper]] probability]]
                             (println (str (inc idx) ": " number " (" (+ 5 lower) " - " (+ 5 upper) ") --- 1 in " (Math/round (/ 1 probability)))))
                           (top-nwrb>=s-in lynch-seq 100)))))