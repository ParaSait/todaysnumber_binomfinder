(ns todaysnumber-binomfinder.binomial
  (:require [todaysnumber-binomfinder.factorial :refer [!]]))

(defn binom
  "https://probabilityformula.org/binomial-probability-formula/"
  [x n p]
  (let [q (- 1 p)
        f (- n x)]
    (-> (! n)
        (/ (* (! x) (! f)))
        (* (Math/pow p x))
        (* (Math/pow q f)))))

(defn binom>=
  [x n p]
  (loop [x* x
         acc 0]
    (if (> x* n)
      acc
      (recur (inc x*)
             (+ acc (binom x* n p))))))