(ns todaysnumber-binomfinder.finder
  (:require [todaysnumber-binomfinder.binomial :refer :all]))

(defn gather-indices
  [pick-seq n]
  (keep-indexed (fn [idx elm]
                  (when (= elm n)
                    idx))
                pick-seq))

(defn gather-index-ranges
  [pick-seq n]
  (let [indices (gather-indices pick-seq n)]
    (loop [ranges '()
           indices* indices]
      (if (empty? indices*)
        ranges
        (recur (concat ranges
                       (let [lower (first indices*)]
                         (map (fn [upper]
                                [lower upper])
                              indices*)))
               (rest indices*))))))

(defn gather-nwrs
  [pick-seq]
  (mapcat (fn [n]
            (map (fn [index-range]
                   [n index-range])
                 (gather-index-ranges pick-seq n)))
          (distinct pick-seq)))

(defn calc-nwr-binom>=
  [[number [lower upper]] pick-seq]
  (let [pick-subvec (subvec (vec pick-seq) lower (inc upper))]
    (binom>= (get (frequencies pick-subvec) number)
             (count pick-subvec)
             0.1)))

(defn calc-nwrb>=
  [nwr pick-seq]
  [nwr (calc-nwr-binom>= nwr pick-seq)])

(defn calc-all-nwrb>=s-in
  ([pick-seq progress-callback]
   (let [nwrs (gather-nwrs pick-seq)
         filtered-nwrs (filter (fn [nwr] (< (- (second (second nwr)) (first (second nwr))) 20)) nwrs) ;; avoid long overflows
         filtered-nwr-count (count filtered-nwrs)]
     (map-indexed (fn [idx nwr]
                    (progress-callback idx filtered-nwr-count)
                    (calc-nwrb>= nwr pick-seq))
                  filtered-nwrs)))
  ([pick-seq]
   (calc-all-nwrb>=s-in pick-seq (fn [current total] nil))))

(defn top-nwrb>=s-in
  [pick-seq top]
  (take top
        (sort-by second
                 (calc-all-nwrb>=s-in pick-seq))))