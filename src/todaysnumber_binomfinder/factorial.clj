(ns todaysnumber-binomfinder.factorial)

(defn !
  [n]
  (loop [n* n
         acc 1]
    (if (= n* 0)
      acc
      (recur (dec n*)
             (* acc n*)))))
